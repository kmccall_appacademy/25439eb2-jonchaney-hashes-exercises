# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = {}
  str.split(" ").each do |word|
    words[word] = word.length
  end
  words
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v,| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k, v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_frequencies = Hash.new(0)
  word.chars.each {|letter| letter_frequencies[letter] += 1}
  letter_frequencies
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  element_frequencies = Hash.new(0)
  arr.each {|el| element_frequencies[el] += 1}
  element_frequencies.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_count = Hash.new(0)
  numbers.each do |num|
    if num.odd?
      parity_count[:odd] += 1
    else
      parity_count[:even] += 1
    end
  end
  parity_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  string.chars.sort.each do |letter|
    if vowel?(letter)
      vowel_count[letter] += 1
    end
  end
  vowel_count.max_by{|k,v| v}[0]
end

def vowel?(letter)
  'aeiou'.include?(letter)
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_birthdays = []
  every_combo = []
  students.each do |k, v|
    if v > 6
      second_half_birthdays << k
    end
  end
  second_half_birthdays.each_with_index do |name1, idx1|
    second_half_birthdays.each_with_index do |name2, idx2|
      next if idx2 <= idx1
      every_combo << [name1,name2]
    end
  end
  every_combo
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens_by_population = Hash.new(0)

  specimens.each do |sp|
    specimens_by_population[sp] += 1
  end

  number_of_species = specimens_by_population.length
  smallest_population_size = specimens_by_population.min_by{|k,v| v}[1]
  largest_population_size = specimens_by_population.max_by{|k,v| v}[1]

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  character_count(vandalized_sign).each do |k,v|
    if character_count(normal_sign)[k] < v
      return false
    end
  end
  true
end

def character_count(str)
  count = Hash.new(0)
  remove_punctuation_and_whitespace(str).chars.each do |ch|
    count[ch] += 1
  end
  count
end

def remove_punctuation_and_whitespace(str)
  str.downcase.delete("\'! ")
end
